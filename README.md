# Hubot - Ansible Role
This role covers deployment, configuration and software updates of Hubot. This role is released under MIT Licence and we give no warranty for this piece of software. Currently supported OS - Debian.

The role allows to install/uninstall Hubot's plugins and to connect bots to your XMPP network.

You can deploy test instance using `Vagrantfile` attached to the role.

`vagrant up`

`ansible-playbook -b Playbooks/hubot.yml`

## Playbook
The playbook includes node role and deploys entire stack needed to run hubot. Additional role is also available in the Ansible roles repos in git.


## XMPP
Each new bot is created as a linux user with in its home path. It needs its own xmpp account.

:warning: The bots' xmpp accounts must be created for the bots to work on the server. For example, check this [Prosody documentation](https://prosody.im/doc/creating_accounts)


## CHANGELOG
- **23.04.2021** - Make it deployable
